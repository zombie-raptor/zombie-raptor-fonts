Zombie Raptor Fonts
===================

This repository contains open source fonts used by
the [Zombie Raptor](https://gitlab.com/zombie-raptor/zombie-raptor)
game engine. This repository exists to be included as a git submodule
in the main zombie-raptor repository. The original git repositories
for the included fonts cannot be used directly as submodules because
that would cause very large downloads (of over a gigabyte) even though
Zombie Raptor doesn't use most of those fonts.

None of these fonts were created by the Zombie Raptor project. They're
used by Zombie Raptor under permission by the included licenses, which
are in the subdirectories.

Included fonts
--------------

- [noto-fonts](https://github.com/googlei18n/noto-fonts)
  - Noto Sans (unhinted)
  - Noto Serif (unhinted)
  - Noto Sans UI (unhinted)
